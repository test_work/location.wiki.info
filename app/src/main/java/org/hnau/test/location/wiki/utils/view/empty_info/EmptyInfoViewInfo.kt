package ru.hnau.remote_teaching_android.ui.empty_info

import org.hnau.test.location.wiki.utils.ColorManager
import org.hnau.test.location.wiki.utils.SizeManager
import org.hnau.test.location.wiki.utils.view.button.LPButtonInfo
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.LabelInfo


data class EmptyInfoViewInfo(
    val title: LabelInfo = LabelInfo(
        textSize = SizeManager.TEXT_20,
        textColor = ColorManager.FG,
        gravity = HGravity.CENTER
    ),
    val buttonInfo: LPButtonInfo = LPButtonInfo.SMALL_PRIMARY_TEXT_AND_BORDER,
    val paddingHorizontal: DpPxGetter = SizeManager.LARGE_SEPARATION,
    val paddingVertical: DpPxGetter = SizeManager.LARGE_SEPARATION,
    val titleButtonSeparation: DpPxGetter = SizeManager.DEFAULT_SEPARATION
) {

    companion object {

        val DEFAULT = EmptyInfoViewInfo()

    }

}