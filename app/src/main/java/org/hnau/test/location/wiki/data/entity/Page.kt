package org.hnau.test.location.wiki.data.entity


data class Page(
    val id: Int,
    val title: String,
    val imagesCount: Int
)