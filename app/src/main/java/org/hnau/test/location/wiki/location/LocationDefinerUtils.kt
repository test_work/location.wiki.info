package org.hnau.test.location.wiki.location

import android.app.Activity
import org.hnau.test.location.wiki.AppActivity
import org.hnau.test.location.wiki.AppActivityConnector
import org.hnau.test.location.wiki.R
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.permissions.PermissionsManager
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


object LocationDefinerUtils {

    suspend fun requestLocationPermission(
        activity: Activity
    ) = PermissionsManager.requestPermission(
        activity = activity,
        permissionName = android.Manifest.permission.ACCESS_FINE_LOCATION,
        showExplanation = this::showLocationPermissionExplanationDialog
    )

    private suspend fun showLocationPermissionExplanationDialog() =
        suspendCoroutine<Unit> { continuation ->
            AppActivityConnector.showDialog {
                title(StringGetter(R.string.location_permission_explanation_dialog_title))
                text(StringGetter(R.string.location_permission_explanation_dialog_text))
                closeButton(StringGetter(R.string.dialog_ok)) { continuation.resume(Unit) }
            }
        }

}