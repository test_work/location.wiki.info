package org.hnau.test.location.wiki.data.wikipedia.dagger

import dagger.Component
import org.hnau.test.location.wiki.AppActivity
import javax.inject.Singleton

@Component(modules = [WikipediaModule::class])
@Singleton
interface WikipediaComponent {

    fun inject(appActivity: AppActivity)

}