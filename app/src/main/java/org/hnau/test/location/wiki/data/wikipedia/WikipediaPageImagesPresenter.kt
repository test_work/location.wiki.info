package org.hnau.test.location.wiki.data.wikipedia

import org.hnau.test.location.wiki.api.API
import org.hnau.test.location.wiki.api.data.GetImagesInfo
import org.hnau.test.location.wiki.data.PageImagesPresenter
import org.hnau.test.location.wiki.data.entity.Image
import org.hnau.test.location.wiki.utils.AsyncCachingProducer
import ru.hnau.jutils.getter.SuspendGetter
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.takeIfNotEmpty


class WikipediaPageImagesPresenter(
    private val pageId: Int
) : AsyncCachingProducer<Unit, List<Image>>(), PageImagesPresenter {

    override val imagesProducer = this

    private var alreadyExistenceImages: List<Image> = emptyList()
    private var lastContinueKey: String? = ""

    override fun loadNextPage() {
        alreadyExistenceImages = existence ?: emptyList()
        forceInvalidate()
    }

    override fun invalidate() {
        alreadyExistenceImages = emptyList()
        lastContinueKey = ""
        super.invalidate()
    }

    override fun getNewGetter() = SuspendGetter.simple {
        alreadyExistenceImages + loadPage()
    }

    private suspend fun loadPage(): List<Image> {
        val lastContinueKey = lastContinueKey ?: return emptyList()
        val getImagesInfo = API().getImages(
            pageId = pageId,
            continueKey = lastContinueKey.takeIfNotEmpty()
        ).await()
        this.lastContinueKey = getImagesInfo.`continue`?.gimcontinue?.takeIfNotEmpty()
        val imageInfos = getImagesInfo.query?.pages?.values ?: return emptyList()
        return imageInfos.map { it.title }.map(::Image)
    }

}