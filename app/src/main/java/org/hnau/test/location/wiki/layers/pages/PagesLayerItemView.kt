package org.hnau.test.location.wiki.layers.pages

import android.content.Context
import org.hnau.test.location.wiki.R
import org.hnau.test.location.wiki.data.entity.Page
import org.hnau.test.location.wiki.utils.ColorManager
import org.hnau.test.location.wiki.utils.SizeManager
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.clickable.ClickableLinearLayout
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.apply.applyPadding
import ru.hnau.androidutils.ui.view.utils.apply.applyVerticalOrientation
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams


class PagesLayerItemView(
    context: Context,
    private val onClick: (Page) -> Unit
) : ClickableLinearLayout(
    context = context,
    rippleDrawInfo = ColorManager.PRIMARY_ON_TRANSPARENT_RIPPLE_INFO
), BaseListViewWrapper<Page> {

    override val view = this

    private val title = Label(
        context = context,
        gravity = HGravity.START_CENTER_VERTICAL,
        maxLines = 3,
        minLines = 1,
        textSize = SizeManager.TEXT_20,
        textColor = ColorManager.PRIMARY
    )

    private val imagesCountInfo = Label(
        context = context,
        gravity = HGravity.START_CENTER_VERTICAL,
        maxLines = 1,
        minLines = 1,
        textSize = SizeManager.TEXT_16,
        textColor = ColorManager.FG.mapWithAlpha(0.5f)
    )
        .applyLinearParams {
            setMatchParentWidth()
            setTopMargin(SizeManager.SMALL_SEPARATION)
        }

    private var page: Page? = null

    init {
        applyVerticalOrientation()
        applyPadding(SizeManager.DEFAULT_SEPARATION)
        addChild(title)
        addChild(imagesCountInfo)
    }

    override fun setContent(content: Page, position: Int) {
        page = content
        title.text = content.title.toGetter()
        imagesCountInfo.text = StringGetter(R.string.images_count, content.imagesCount)
    }

    override fun onClick() {
        super.onClick()
        page?.let(onClick)
    }

}