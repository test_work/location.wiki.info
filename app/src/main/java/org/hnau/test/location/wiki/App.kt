package org.hnau.test.location.wiki

import android.app.Application
import org.hnau.test.location.wiki.data.wikipedia.dagger.DaggerWikipediaComponent
import ru.hnau.androidutils.utils.ContextConnector


class App : Application() {

    companion object {

        val pagesManagerComponent =
            DaggerWikipediaComponent
                .builder()
                .build()

    }

    override fun onCreate() {
        super.onCreate()
        ContextConnector.init(this)
    }

}