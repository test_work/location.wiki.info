package org.hnau.test.location.wiki.data.wikipedia

import android.app.Activity
import org.hnau.test.location.wiki.api.API
import org.hnau.test.location.wiki.api.data.GetPagesInfo
import org.hnau.test.location.wiki.data.PagesPresenter
import org.hnau.test.location.wiki.data.entity.Page
import org.hnau.test.location.wiki.location.LocationDefiner
import org.hnau.test.location.wiki.utils.AsyncCachingProducer
import ru.hnau.jutils.getter.SuspendGetter


class WikipediaPagesPresenter :
    AsyncCachingProducer<Activity, List<Page>>(),
    PagesPresenter {

    companion object {
        private const val SEARCH_REDIUS = 10000
        private const val PAGES_LIMIT = 50
    }

    override val pagesProducer = this

    override fun getNewGetter() =
        SuspendGetter<Activity, List<Page>> { activity ->
            val location = LocationDefiner.define(activity)
            API()
                .getPages(
                    location = "${location.latitude}|${location.longitude}",
                    searchRadius = SEARCH_REDIUS,
                    pagesLimit = PAGES_LIMIT
                )
                .await()
                .query
                ?.pages
                ?.map { (id, pageInfo) ->
                    Page(
                        id = id,
                        imagesCount = pageInfo.images?.size ?: 0,
                        title = pageInfo.title
                    )
                }
                ?: emptyList()
        }


}