package org.hnau.test.location.wiki.location

import android.content.Context
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import ru.hnau.jutils.producer.Producer


class LocationProducer(
    private val context: Context
) : Producer<Location>(), LocationListener {

    private val CRITERIA = Criteria().apply {
        accuracy = Criteria.ACCURACY_MEDIUM
        powerRequirement = Criteria.POWER_HIGH
    }

    private val locationManager: LocationManager
        get() = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    override fun onFirstAttached() {
        super.onFirstAttached()
        locationManager.requestSingleUpdate(Criteria(), this, Looper.getMainLooper())
    }

    override fun onLastDetached() {
        super.onLastDetached()
        locationManager.removeUpdates(this)
    }

    override fun onLocationChanged(location: Location) =
        call(location)

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

    override fun onProviderEnabled(provider: String?) {}

    override fun onProviderDisabled(provider: String?) {}

}