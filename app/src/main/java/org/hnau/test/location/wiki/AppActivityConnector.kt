package org.hnau.test.location.wiki

import org.hnau.test.location.wiki.api.data.PageInfo
import org.hnau.test.location.wiki.data.entity.Page
import org.hnau.test.location.wiki.layers.images.ImagesLayer
import org.hnau.test.location.wiki.utils.DialogManager
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.MaterialDialogView


object AppActivityConnector {

    var appActivity: AppActivity? = null
        private set

    fun onAppActivityCreate(appActivity: AppActivity) {
        this.appActivity = appActivity
    }

    fun onAppActivityDestroy(appActivity: AppActivity) =
        synchronized(this) {
            if (appActivity == this.appActivity) {
                this.appActivity = null
            }
        }

    fun showDialog(dialogViewConfigurator: MaterialDialogView.() -> Unit) {
        appActivity?.let { appActivity ->
            DialogManager.showDialog(
                appActivity.layerManagerConnector,
                dialogViewConfigurator
            )
        }
    }

    fun onPageClick(page: Page) {
        appActivity?.let { appActivity ->
            appActivity.layerManagerConnector.showLayer(
                ImagesLayer.newInstance(
                    context = appActivity,
                    imagesPresenter = appActivity.imagesPresenter.getPageImages(pageId = page.id)
                )
            )
        }
    }

}