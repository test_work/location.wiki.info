package org.hnau.test.location.wiki.layers.images

import android.content.Context
import android.widget.LinearLayout
import org.hnau.test.location.wiki.data.entity.Image
import org.hnau.test.location.wiki.utils.ColorManager
import org.hnau.test.location.wiki.utils.SizeManager
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.clickable.ClickableLinearLayout
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.apply.applyPadding
import ru.hnau.androidutils.ui.view.utils.apply.applyVerticalOrientation
import ru.hnau.androidutils.utils.ContextConnector.context
import ru.hnau.androidutils.utils.ContextConnector.init


class ImagesLayerItemView(
    context: Context,
    private val onBind: (Int) -> Unit
) : LinearLayout(
    context
), BaseListViewWrapper<Image> {

    override val view = this

    private val title = Label(
        context = context,
        gravity = HGravity.START_CENTER_VERTICAL,
        maxLines = 3,
        minLines = 1,
        textSize = SizeManager.TEXT_20,
        textColor = ColorManager.PRIMARY
    )

    init {
        applyVerticalOrientation()
        applyPadding(SizeManager.DEFAULT_SEPARATION)
        addChild(title)
    }

    override fun setContent(content: Image, position: Int) {
        title.text = content.title.toGetter()
        onBind(position)
    }

}