package org.hnau.test.location.wiki.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import org.hnau.test.location.wiki.api.data.GetImagesInfo
import org.hnau.test.location.wiki.api.data.GetPagesInfo
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface API {

    companion object {

        private const val HOST = "https://ru.wikipedia.org/"

        private val INSTANCE = Retrofit.Builder()
            .baseUrl(HOST)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(API::class.java)

        operator fun invoke() = INSTANCE

    }

    @GET("/w/api.php?action=query&generator=geosearch&format=json&prop=images&imlimit=max")
    fun getPages(
        @Query("ggscoord") location: String,
        @Query("ggslimit") pagesLimit: Int,
        @Query("ggsradius") searchRadius: Int
    ): Deferred<GetPagesInfo>

    @GET("/w/api.php?action=query&format=json&generator=images")
    fun getImages(
        @Query("pageids") pageId: Int,
        @Query("gimcontinue") continueKey: String?
    ): Deferred<GetImagesInfo>

}