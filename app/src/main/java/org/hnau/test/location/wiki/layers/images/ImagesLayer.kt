package org.hnau.test.location.wiki.layers.images

import android.content.Context
import org.hnau.test.location.wiki.data.PageImagesPresenter
import org.hnau.test.location.wiki.data.PagesPresenter
import org.hnau.test.location.wiki.data.entity.Image
import org.hnau.test.location.wiki.data.entity.Page
import org.hnau.test.location.wiki.layers.base.ListLayer
import org.hnau.test.location.wiki.layers.pages.PagesLayer
import ru.hnau.androidutils.ui.view.layer.layer.LayerState
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.producer.Producer


class ImagesLayer(
    context: Context
) : ListLayer<Image>(context) {

    companion object {

        fun newInstance(
            context: Context,
            imagesPresenter: PageImagesPresenter
        ) = ImagesLayer(
            context = context
        ).apply {
            this.imagesPresenter = imagesPresenter
        }

    }

    @LayerState
    private lateinit var imagesPresenter: PageImagesPresenter

    private var itemsCount = 0

    init {
        itemsProducerSync.attach { itemsCount = it.size }
    }

    override val itemsProducer: Producer<GetterAsync<Unit, List<Image>>>
        get() = imagesPresenter.imagesProducer

    override fun invalidateContent() =
        imagesPresenter.imagesProducer.invalidate()

    override fun createViewWrapper() =
        ImagesLayerItemView(context, this::onItemBind)

    private fun onItemBind(position: Int) {
        if (position >= itemsCount - 1) {
            imagesPresenter.loadNextPage()
        }
    }

}