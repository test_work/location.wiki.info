package org.hnau.test.location.wiki.layers.base

import android.content.Context
import org.hnau.test.location.wiki.utils.ColorManager
import org.hnau.test.location.wiki.utils.SizeManager
import ru.hnau.androidutils.context_getters.dp_px.dp1
import ru.hnau.androidutils.ui.view.list.base.*
import ru.hnau.jutils.producer.Producer


class ListLayerItemsList<T>(
    context: Context,
    items: Producer<List<T>>,
    viewWrappersCreator: () -> BaseListViewWrapper<T>
) : BaseList<T>(
    context = context,
    orientation = BaseListOrientation.VERTICAL,
    itemsProducer = items,
    viewWrappersCreator = { viewWrappersCreator() },
    itemsDecoration = BaseListItemsDivider(
        context = context,
        color = ColorManager.FG.mapWithAlpha(0.25f),
        size = dp1,
        paddingStart = SizeManager.DEFAULT_SEPARATION
    )
)