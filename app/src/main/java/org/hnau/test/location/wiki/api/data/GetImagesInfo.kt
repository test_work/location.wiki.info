package org.hnau.test.location.wiki.api.data

data class ImageInfo(
    val title: String
)

data class ImagesInfoQuery(
    val pages: Map<Int, ImageInfo>?
)

data class ImagesInfoContinue(
    val gimcontinue: String?
)

data class GetImagesInfo(
    val `continue`: ImagesInfoContinue?,
    val query: ImagesInfoQuery?
)