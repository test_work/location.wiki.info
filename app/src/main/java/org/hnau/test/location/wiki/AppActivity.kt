package org.hnau.test.location.wiki

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import org.hnau.test.location.wiki.data.ImagesPresenter
import org.hnau.test.location.wiki.data.PagesPresenter
import org.hnau.test.location.wiki.layers.pages.PagesLayer
import ru.hnau.androidutils.ui.utils.permissions.PermissionsManager
import ru.hnau.androidutils.ui.view.layer.manager.LayerManager
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.utils.setFitKeyboard
import javax.inject.Inject

class AppActivity : AppCompatActivity() {

    @Inject
    lateinit var pagesPresenter: PagesPresenter

    @Inject
    lateinit var imagesPresenter: ImagesPresenter

    init {
        App.pagesManagerComponent.inject(this)
    }

    private val context: Context
        get() = this

    private val layerManager: LayerManager by lazy {
        val layerManager = LayerManager(context).apply {
            showLayer(PagesLayer.newInstance(context, pagesPresenter))
            setFitKeyboard()
        }
        return@lazy layerManager
    }

    val layerManagerConnector: LayerManagerConnector
        get() = layerManager

    val contentView: View
        get() = layerManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layerManager)
        AppActivityConnector.onAppActivityCreate(this)
    }

    override fun onDestroy() {
        AppActivityConnector.onAppActivityDestroy(this)
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionsManager.onRequestResult(requestCode, permissions, grantResults)
    }

    override fun onBackPressed() {
        if (layerManager.handleGoBack()) {
            return
        }
        super.onBackPressed()
    }

}
