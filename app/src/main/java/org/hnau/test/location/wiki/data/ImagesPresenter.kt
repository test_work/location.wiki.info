package org.hnau.test.location.wiki.data


interface ImagesPresenter {

    fun getPageImages(pageId: Int): PageImagesPresenter

}