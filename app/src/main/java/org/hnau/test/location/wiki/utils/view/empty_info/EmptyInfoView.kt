package ru.hnau.remote_teaching_android.ui.empty_info

import android.annotation.SuppressLint
import android.content.Context
import android.widget.LinearLayout
import org.hnau.test.location.wiki.R
import org.hnau.test.location.wiki.utils.view.button.addLPButton
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.label.addLabel
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.ui.view.utils.apply.applyCenterGravity
import ru.hnau.androidutils.ui.view.utils.apply.applyPadding
import ru.hnau.androidutils.ui.view.utils.apply.layout_params.applyLinearParams


@SuppressLint("ViewConstructor")
class EmptyInfoView(
    context: Context,
    text: StringGetter,
    button: Pair<StringGetter, () -> Unit>? = null,
    info: EmptyInfoViewInfo = EmptyInfoViewInfo.DEFAULT
) : LinearLayout(context) {

    companion object {

        fun createLoadError(
            context: Context,
            info: EmptyInfoViewInfo = EmptyInfoViewInfo.DEFAULT,
            onButtonClick: () -> Unit
        ) = EmptyInfoView(
            context = context,
            info = info,
            text = StringGetter(R.string.error_load_title),
            button = StringGetter(R.string.error_load_reload) to onButtonClick
        )

    }

    init {
        orientation = VERTICAL
        applyPadding(info.paddingHorizontal, info.paddingVertical)
        applyCenterGravity()

        addLabel(
            text = text,
            info = info.title
        )

        button?.let { (title, onClick) ->

            addLPButton(
                text = title,
                info = info.buttonInfo,
                onClick = onClick
            ) {
                applyLinearParams {
                    setTopMargin(info.titleButtonSeparation)
                }
            }

        }

    }

}