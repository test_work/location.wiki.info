package org.hnau.test.location.wiki.location

import android.app.Activity
import android.location.Location
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.coroutines.withTimeout


object LocationDefiner {

    private val TIME_LIMIT = TimeValue.SECOND * 20

    suspend fun define(
        activity: Activity
    ): Location {
        LocationDefinerUtils.requestLocationPermission(activity)
        return withTimeout(TIME_LIMIT) { LocationProducer(activity).wait() }
    }

}