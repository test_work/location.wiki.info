package org.hnau.test.location.wiki.data

import android.app.Activity
import org.hnau.test.location.wiki.api.data.GetImagesInfo
import org.hnau.test.location.wiki.api.data.GetPagesInfo
import org.hnau.test.location.wiki.data.entity.Image
import org.hnau.test.location.wiki.utils.AsyncCachingProducer


interface PageImagesPresenter {

    val imagesProducer: AsyncCachingProducer<Unit, List<Image>>

    fun loadNextPage()

}