package org.hnau.test.location.wiki.layers.pages

import android.app.Activity
import android.content.Context
import android.view.View
import org.hnau.test.location.wiki.AppActivityConnector
import org.hnau.test.location.wiki.R
import org.hnau.test.location.wiki.api.data.GetPagesInfo
import org.hnau.test.location.wiki.api.data.PageInfo
import org.hnau.test.location.wiki.data.PagesPresenter
import org.hnau.test.location.wiki.data.entity.Page
import org.hnau.test.location.wiki.layers.base.ListLayer
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.permissions.OnPermissionDeniedException
import ru.hnau.androidutils.ui.utils.permissions.OnPermissionDeniedForeverException
import ru.hnau.androidutils.ui.view.layer.layer.LayerState
import ru.hnau.jutils.getter.SuspendGetter
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.producer.Producer
import ru.hnau.remote_teaching_android.ui.empty_info.EmptyInfoView
import ru.hnau.remote_teaching_android.ui.empty_info.EmptyInfoViewInfo


class PagesLayer(
    context: Context
) : ListLayer<Page>(context) {

    companion object {

        fun newInstance(
            context: Context,
            pagesPresenter: PagesPresenter
        ) = PagesLayer(
            context = context
        ).apply {
            this.pagesPresenter = pagesPresenter
        }

    }

    @LayerState
    private lateinit var pagesPresenter: PagesPresenter

    override val itemsProducer: Producer<GetterAsync<Unit, List<Page>>> by lazy {
        pagesPresenter.pagesProducer.map { getter ->
            SuspendGetter.simple { getter.get(context as Activity) }
        } as Producer<GetterAsync<Unit, List<Page>>>
    }

    override fun invalidateContent() =
        pagesPresenter.pagesProducer.invalidate()

    override fun createViewWrapper() =
        PagesLayerItemView(context, AppActivityConnector::onPageClick)

    override fun generateErrorView(error: Throwable) =
        when (error) {
            is OnPermissionDeniedException ->
                EmptyInfoView(
                    context = context,
                    info = EmptyInfoViewInfo.DEFAULT,
                    text = StringGetter(R.string.location_permission_denied_error_text),
                    button = StringGetter(R.string.location_permission_denied_error_button_text) to pagesPresenter.pagesProducer::invalidate
                )
            is OnPermissionDeniedForeverException ->
                EmptyInfoView(
                    context = context,
                    info = EmptyInfoViewInfo.DEFAULT,
                    text = StringGetter(R.string.location_permission_denied_forever_error_text),
                    button = StringGetter(R.string.location_permission_denied_forever_error_button_text) to {
                        invalidateContent()
                        error.openSettingsToGivePermission(context)
                    }
                )
            else -> super.generateErrorView(error)
        }

}