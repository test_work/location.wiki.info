package org.hnau.test.location.wiki.utils

import android.util.Range
import org.hnau.test.location.wiki.R
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.layer.preset.dialog.DialogLayer
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.BottomSheetView
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.BottomSheetViewInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.item.BottomSheetItemInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.text.BottomSheetTextInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.title.BottomSheetTitleInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.MaterialDialogView
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.MaterialDialogViewInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.button.MaterialDialogButtonInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.text.MaterialDialogTextInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.title.MaterialDialogTitleInfo
import ru.hnau.androidutils.ui.view.layer.transaction.TransactionInfo
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.utils.KeyboardManager
import ru.hnau.jutils.TimeValue


object DialogManager {

    fun showDialog(
        layerManager: LayerManagerConnector,
        dialogViewConfigurator: MaterialDialogView.() -> Unit
    ) {
        val dialogBuilder = MaterialDialogView.create(
            dialogViewConfigurator = dialogViewConfigurator,
            info = MaterialDialogViewInfo.DEFAULT
        )
        layerManager.showLayer(
            layer = DialogLayer.create(
                context = layerManager.viewContext,
                dialogViewBuilder = dialogBuilder
            ),
            transactionInfo = TransactionInfo(
                emersionSide = Side.BOTTOM
            )
        )
    }

}