package org.hnau.test.location.wiki.utils

import android.content.Context
import android.view.View
import org.hnau.test.location.wiki.R
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.dp2
import ru.hnau.androidutils.context_getters.dp_px.dp4
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleInfo
import ru.hnau.androidutils.ui.drawer.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.drawer.shadow.info.ShadowInfo
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.view_presenter.PresenterViewInfo
import ru.hnau.androidutils.ui.view.view_presenter.PresentingViewProperties
import ru.hnau.androidutils.ui.view.waiter.material.MaterialWaiterView
import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterColor
import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterSize
import ru.hnau.androidutils.utils.ContextConnector.context
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.locked_producer.LockedProducer


object ColorManager {

    val PRIMARY = ColorGetter.byResId(R.color.primary)
    val BG = ColorGetter.WHITE
    val FG = ColorGetter.BLACK

    val RIPPLE_INFO = RippleInfo()

    private const val RIPPLE_ALPHA = 0.1f

    val PRIMARY_ON_TRANSPARENT_RIPPLE_INFO = RippleDrawInfo(
        rippleInfo = RIPPLE_INFO,
        backgroundColor = ColorGetter.TRANSPARENT,
        color = PRIMARY,
        rippleAlpha = RIPPLE_ALPHA
    )

    val DEFAULT_SHADOW_INFO = ShadowInfo(dp4, dp8, ColorGetter.BLACK, 0.4f)
    val DEFAULT_PRESSED_SHADOW_INFO = ShadowInfo(dp2, dp4, ColorGetter.BLACK, 0.4f)
    val DEFAULT_BUTTON_SHADOW_INFO = ButtonShadowInfo(
        normal = DEFAULT_SHADOW_INFO,
        pressed = DEFAULT_PRESSED_SHADOW_INFO,
        animationTime = TimeValue.MILLISECOND * 100
    )


    val PRESENTING_VIEW_PROPERTIES = PresentingViewProperties(
        fromSide = Side.BOTTOM,
        scrollFactor = 0.25f
    )

    fun generateWaiterView(
        context: Context,
        lockedProducer: LockedProducer
    ) = MaterialWaiterView(
        context = context,
        lockedProducer = lockedProducer,
        size = MaterialWaiterSize.LARGE,
        color = MaterialWaiterColor(
            foreground = PRIMARY,
            background = BG.mapWithAlpha(0.5f)
        )
    )

}