package org.hnau.test.location.wiki.layers.base

import android.content.Context
import android.widget.FrameLayout
import org.hnau.test.location.wiki.utils.ColorManager
import ru.hnau.androidutils.ui.view.auto_swipe_refresh_view.addAutoSwipeRefreshView
import ru.hnau.androidutils.ui.view.layer.layer.Layer
import ru.hnau.androidutils.ui.view.layer.layer.LayerState
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.apply.applyBackground
import ru.hnau.androidutils.ui.view.view_presenter.*
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.StateProducerSimple
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.remote_teaching_android.ui.empty_info.EmptyInfoView
import ru.hnau.remote_teaching_android.ui.empty_info.EmptyInfoViewInfo
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import ru.hnau.androidutils.utils.ContextConnector.context
import ru.hnau.jutils.takeIfPositive


abstract class ListLayer<T>(
    context: Context
) : FrameLayout(context), Layer {

    @LayerState
    private var listPosition: Int = 0

    override val view = this

    protected abstract val itemsProducer: Producer<GetterAsync<Unit, List<T>>>

    protected abstract fun invalidateContent()

    protected abstract fun createViewWrapper(): BaseListViewWrapper<T>

    private val itemsProducerSyncInner = StateProducerSimple<List<T>>()

    protected val itemsProducerSync: Producer<List<T>>
        get() = itemsProducerSyncInner

    private val list: RecyclerView by lazy {
        ListLayerItemsList(
            context = context,
            items = itemsProducerSyncInner,
            viewWrappersCreator = this::createViewWrapper
        )
    }

    private val listPresentingInfo: PresentingViewInfo by lazy {
        list.toPresentingInfo(ColorManager.PRESENTING_VIEW_PROPERTIES)
    }

    init {
        applyBackground(ColorManager.BG)
    }

    override fun beforeDestroy() {
        super.beforeDestroy()
        listPosition = getListPosition() ?: 0
    }

    override fun afterCreate() {
        super.afterCreate()

        addAutoSwipeRefreshView(
            color = ColorManager.PRIMARY,
            updateContent = this::invalidateContent
        ) {

            addChild(
                SuspendPresenterView.create<List<T>>(
                    context = context,
                    producer = itemsProducer as Producer<GetterAsync<Unit, List<T>>>,
                    contentViewGenerator = this@ListLayer::generateContentView,
                    errorViewGenerator = { generateErrorView(it).toPresentingInfo(ColorManager.PRESENTING_VIEW_PROPERTIES) },
                    waiterViewGenerator = this@ListLayer::generateWaiterView,
                    presenterViewInfo = PresenterViewInfo(
                        horizontalSizeInterpolator = SizeInterpolator.MAX,
                        verticalSizeInterpolator = SizeInterpolator.MAX
                    )
                )
            )

        }


    }

    private fun getListPosition() =
        (list.layoutManager as? LinearLayoutManager)
            ?.findFirstVisibleItemPosition()
            ?.takeIf { it != RecyclerView.NO_POSITION }

    private fun setListPosition(position: Int?) {
        (list.layoutManager as? LinearLayoutManager)?.let { layoutManager ->
            position
                ?.takeIf { it < layoutManager.itemCount }
                ?.let {
                    layoutManager.scrollToPositionWithOffset(it, 0)
                }
        }
    }

    private fun generateContentView(data: List<T>): PresentingViewInfo {
        itemsProducerSyncInner.updateState(data)
        listPosition.takeIfPositive()?.let {
            post {
                setListPosition(it)
                listPosition = 0
            }
        }
        return listPresentingInfo
    }

    protected open fun generateErrorView(error: Throwable) =
        EmptyInfoView.createLoadError(
            context = context,
            info = EmptyInfoViewInfo.DEFAULT,
            onButtonClick = this::invalidateContent
        )

    private fun generateWaiterView(lockedProducer: LockedProducer) =
        ColorManager.generateWaiterView(context, lockedProducer)

}