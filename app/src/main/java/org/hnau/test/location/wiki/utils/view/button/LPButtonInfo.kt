package org.hnau.test.location.wiki.utils.view.button

import org.hnau.test.location.wiki.utils.ColorManager
import org.hnau.test.location.wiki.utils.SizeManager
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.*
import ru.hnau.androidutils.ui.drawer.border.BorderInfo
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.shadow.info.ButtonShadowInfo


data class LPButtonInfo(
    val textSize: DpPxGetter = SizeManager.TEXT_20,
    val shadow: ButtonShadowInfo? = null,
    val height: DpPxGetter,
    val paddingHorizontal: DpPxGetter = height,
    val borderInfo: BorderInfo? = null,
    val textColor: ColorGetter,
    val underline: Boolean = false,
    val backgroundColor: ColorGetter
) {

    companion object {

        val LARGE_PRIMARY_BACKGROUND_SHADOW = LPButtonInfo(
            shadow = ColorManager.DEFAULT_BUTTON_SHADOW_INFO,
            height = dp48,
            textColor = ColorManager.BG,
            backgroundColor = ColorManager.PRIMARY
        )

        val SMALL_FG_TEXT_UNDERLINE = LPButtonInfo(
            textSize = SizeManager.TEXT_16,
            height = dp40,
            textColor = ColorManager.FG,
            backgroundColor = ColorManager.BG,
            underline = true
        )

        val SMALL_PRIMARY_TEXT_AND_BORDER =
            LPButtonInfo(
                textSize = SizeManager.TEXT_16,
                height = dp40,
                textColor = ColorManager.PRIMARY,
                backgroundColor = ColorManager.BG,
                borderInfo = createBorderInfo(
                    ColorManager.PRIMARY
                )
            )

        private fun createBorderInfo(color: ColorGetter) =
            BorderInfo(
                color = color,
                alpha = 1f,
                width = dp2
            )

    }

    val rippleDrawInfo = RippleDrawInfo(
        rippleInfo = ColorManager.RIPPLE_INFO,
        backgroundColor = backgroundColor,
        color = textColor
    )

}