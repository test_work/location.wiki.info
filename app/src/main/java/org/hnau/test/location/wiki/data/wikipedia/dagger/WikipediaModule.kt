package org.hnau.test.location.wiki.data.wikipedia.dagger

import dagger.Module
import dagger.Provides
import org.hnau.test.location.wiki.data.ImagesPresenter
import org.hnau.test.location.wiki.data.PagesPresenter
import org.hnau.test.location.wiki.data.wikipedia.WikipediaImagesPresenter
import org.hnau.test.location.wiki.data.wikipedia.WikipediaPagesPresenter
import javax.inject.Singleton

@Module
class WikipediaModule {

    @Provides
    @Singleton
    fun providePagesPresenter(): PagesPresenter =
        WikipediaPagesPresenter()

    @Provides
    @Singleton
    fun provideImagesPresenter(): ImagesPresenter =
        WikipediaImagesPresenter()

}