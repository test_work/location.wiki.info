package org.hnau.test.location.wiki.data

import android.app.Activity
import org.hnau.test.location.wiki.api.data.GetPagesInfo
import org.hnau.test.location.wiki.data.entity.Page
import org.hnau.test.location.wiki.utils.AsyncCachingProducer
import ru.hnau.jutils.getter.SuspendGetter
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.producer.CachingProducer
import ru.hnau.jutils.producer.Producer


interface PagesPresenter {

    val pagesProducer: AsyncCachingProducer<Activity, List<Page>>

}