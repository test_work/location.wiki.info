package org.hnau.test.location.wiki.data.wikipedia

import org.hnau.test.location.wiki.data.ImagesPresenter
import org.hnau.test.location.wiki.data.PageImagesPresenter
import ru.hnau.androidutils.utils.AutokeyMapCache
import ru.hnau.jutils.cache.AutoCache


class WikipediaImagesPresenter : AutoCache<Int, PageImagesPresenter>(
    capacity = 1000,
    getter = ::WikipediaPageImagesPresenter
), ImagesPresenter {

    override fun getPageImages(pageId: Int) = get(pageId)

}