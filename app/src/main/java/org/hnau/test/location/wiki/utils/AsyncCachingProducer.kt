package org.hnau.test.location.wiki.utils

import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.handle
import ru.hnau.jutils.helpers.Outdatable
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.tryOrElse


abstract class AsyncCachingProducer<P, V : Any> : Producer<GetterAsync<P, V>>(), GetterAsync<P, V> {

    private var attached = false

    private var cachedGetter: GetterAsync<P, V>? = null
        set(value) {
            field = value
            cachedResult = null
            cachedError = null
        }

    private var cachedResult: V? = null
    private var cachedError: Throwable? = null

    protected val existence: V?
        get() = cachedResult

    override suspend fun get(param: P): V {
        cachedError?.let { throw it }
        cachedResult?.let { return it }
        return try {
            val result = getGetter().get(param)
            cachedResult = result
            result
        } catch (th: Throwable) {
            cachedError = th
            throw th
        }
    }


    open fun invalidate() = synchronized(this) {
        attached.handle(
            onTrue = this::forceInvalidate,
            onFalse = this::clear
        )
    }

    fun forceInvalidate() =
        update(getNewGetter())

    fun clear() =
        update(null)

    fun update(
        value: GetterAsync<P, V>?
    ) = synchronized<Unit>(this) {
        cachedGetter = value
        value?.let { call(this) }
    }

    private fun getGetter(): GetterAsync<P, V> {
        var result = cachedGetter
        if (result == null) {
            result = getNewGetter()
            cachedGetter = result
        }
        return result
    }

    protected abstract fun getNewGetter(): GetterAsync<P, V>

    override fun onAttached(
        listener: (GetterAsync<P, V>) -> Unit
    ) {
        super.onAttached(listener)
        synchronized(this) {
            cachedGetter.handle(
                ifNotNull = { listener(this) },
                ifNull = this::forceInvalidate
            )
        }
    }

    override fun onFirstAttached() {
        super.onFirstAttached()
        attached = true
    }

    override fun onLastDetached() {
        attached = false
        super.onLastDetached()
    }

}