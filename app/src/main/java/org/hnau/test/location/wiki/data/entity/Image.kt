package org.hnau.test.location.wiki.data.entity


data class Image(
    val title: String
)