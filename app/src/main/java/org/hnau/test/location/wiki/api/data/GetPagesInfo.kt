package org.hnau.test.location.wiki.api.data


data class PageInfo(
    val title: String,
    val pageid: Int,
    val images: Array<Any>?
)

data class PagesInfo(
    val pages: Map<Int, PageInfo>?
)

data class GetPagesInfo(
    val query: PagesInfo?
)